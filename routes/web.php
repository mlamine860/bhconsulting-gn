<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\ArticleController;
use App\Http\Controllers\admin\ServiceController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\AdminLayout;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CategorieController;
use App\Http\Controllers\CommentaireController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VisiteurController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//UI ROUTES
Route::get('/', [MainController::class, 'index'])->name('index');

Route::get('/articles', [MainController::class, 'article'])->name('articles');
Route::get('/articles/{slug}', [MainController::class, 'voirplus'])->name('article');

Route::get('/services', [MainController::class, 'services'])->name('services');
Route::post('/services', [CommentaireController::class, 'store'])->name('commentaireStore');
Route::get('/services/{slug}', [MainController::class, 'service'])->name('service');

Route::get('/apropos', [MainController::class, 'apropos'])->name('apropos');

Route::get('/contact', [ContactController::class, 'contact'])->name('contact');
Route::get('/ajouter-nouveau-contact', [ContactController::class, 'newcontact'])->name('newcontact');
Route::post('/contact', [ContactController::class, 'store'])->name('contactStore');


// Group of all admin routes and apply appropriate  middleware [admin,]
Route::middleware(['admin'])->group(function () {
    //ADMIN ROUTE
    Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
    Route::get('admin/articles', [ArticleController::class, 'index'])->name('adminArticle');
    Route::get('admin/services', [ServiceController::class, 'index'])->name('adminService');
    Route::get('admin/users', [UserController::class, 'index'])->name('users');
    Route::get('admin/visiteurs', [ContactController::class, 'contact'])->name('visiteurs');
    Route::get('admin/control', [AdminLayout::class, 'adminLayout'])->name('admin');
    Route::get('admin/commentaires', [CommentaireController::class, 'index'])->name('commentaire');
    Route::get('admin/categories', [CategorieController::class, 'index'])->name('categorie');
    Route::post('admin/categories', [CategorieController::class, 'store'])->name('categorieStore');

    //EDIT
    Route::get('/admin/articles/{articles}/edit', [ArticleController::class, 'edit'])->name('articles.edit');
    Route::put('/admin/articles/{articles}/update', [ArticleController::class, 'update'])->name('articles.update');
    Route::get('/admin/services/{services}/edit', [ServiceController::class, 'edit'])->name('services.edit');
    Route::put('/admin/services/{services}/update', [ServiceController::class, 'update'])->name('services.update');
    Route::get('/admin/categories/{categories}/edit', [CategorieController::class, 'edit'])->name('categories.edit');
    Route::put('/admin/categories/{categories}/update', [CategorieController::class, 'update'])->name('categories.update');
    //DELETE
    Route::delete('/admin/articles/{articles}/delete', [ArticleController::class, 'destroy'])->name('articles.delete');
    Route::delete('/admin/services/{services}/delete', [ServiceController::class, 'destroy'])->name('services.delete');
    Route::delete('/admin/categories/{categories}/delete', [CategorieController::class, 'destroy'])->name('categories.delete');

    //FORMS
    Route::get('admin/user_create', [UserController::class, 'create'])->name('userCreate');
    Route::post('admin/user_create', [UserController::class, 'store'])->name('userStore');

    Route::get('admin/article_create', [ArticleController::class, 'create'])->name('articleCreate');
    Route::post('admin/article_create', [ArticleController::class, 'store'])->name('articleStore');

    Route::get('admin/service_create', [ServiceController::class, 'service'])->name('serviceCreate');
    Route::post('admin/service_create', [ServiceController::class, 'store'])->name('serviceStore');


    //DETAIL ROUTES
    Route::get('/admin/articles/{slug}', [ArticleController::class, 'voirplus'])->name('articleDetail');
    Route::get('/admin/services/{slug}', [ServiceController::class, 'voirplus'])->name('serviceDetail');

    // Toggle user permission
    Route::get('admin/users/{user}/toggle-admin', [UserController::class, 'toggleAdmin'])->name('user.toggleAdmin');
    Route::get('admin/users/{user}/destroy', [UserController::class, 'destroy'])->name('user.destroy');
});






Auth::routes();

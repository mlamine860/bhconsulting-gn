@extends('base')
@section('content')
    


  <main id="main">

    <section class="hero-section inner-page">
      <div class="wave">

        <svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
              <path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z" id="Path"></path>
            </g>
          </g>
        </svg>

      </div>

      <div class="container">
        <div class="row align-items-center">
          <div class="col-12">
            <div class="row justify-content-center">
              <div class="col-md-7 text-center hero-text">
                <h1 data-aos="fade-up" data-aos-delay="">Dites nous de quoi avez-vous besoin</h1>
                <p class="mb-5" data-aos="fade-up" data-aos-delay="100">Laissez nous message pour tous vos besoins en entrant vos informations dans ce formulaire</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>

    <section class="section">
      <div class="container">
        <div class="row mb-5 align-items-end">
          <div class="col-md-6" data-aos="fade-up">

            <h2>Dites nous ceux dont vous avez besoin conformement à nos services</h2>
            <p class="mb-0">Veuillez nous contacter pour plus d'information à propos de votre inquietude.</p>
          </div>

        </div>

        <div class="row">
          <div class="col-md-6 mb-5 mb-md-0" data-aos="fade-up">
            <script>
              if (document.getElementById('contact-form').submit()) {
                alert("Message bien envoyé");
              }
            </script>
            <div>
              <form action=" {{ route('contactStore') }}" method="POST" id="contact-form">
                @csrf
                <div class="form-group has-error">
                  <label for="nom_complet" class="control-label">Nom complet</label>
                  <input type="text" name="nom_complet" class="form-control">
                </div>
                <div class="form-group has-error">
                  <label for="email" class="control-label">E-mail</label>
                  <input type="email" name="email" class="form-control">
                </div>
                <div class="form-group has-error">
                  <label for="telephone" class="control-label">Téléphone</label>
                  <input type="text" name="telephone" class="form-control">
                </div>
                <div class="form-group">
                  <label for="objet" class="control-label">Sujet</label>
                  <input type="text" name="objet" class="form-control" >
                </div>
                <div>
                  <label for="message" class="control-label">Message</label>
                  <textarea name="message" id="" class="form-control"></textarea>
                </div>
                <div>
                  <input type="submit" id="submit" value="Envoyer" class="btn btn-success mt-3">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ======= CTA Section ======= -->

  </main><!-- End #main -->

@endsection

@extends('admin.base')

@section('content')
        <div class="">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-12">
                        <!-- general form elements -->
                        <div class="card card-success ">
                            <div class="card-header">
                                <h3 class="card-title">Modifer les informations</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="POST" action="{{route('categories.update', $categories->id)}}">
                                @method('PUT')
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="">Titre</label>
                                        <input type="text" value="{{$categories->categories}}" name="categorie" class="form-control" id="exampleInputEmail1"
                                            placeholder="Entrer la categorie">
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-success">Modifer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
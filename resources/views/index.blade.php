@extends('base')

@section('content')
<section class="hero-section" id="hero">
    <div class="wave">
        <svg width="100%" height="355px" viewBox="0 0 1920 355" version="1.1" xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink">
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
                    <path
                        d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,757 L1017.15166,757 L0,757 L0,439.134243 Z"
                        id="Path"></path>
                </g>
            </g>
        </svg>
    </div>

    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 hero-text-image">
                <div class="row">
                    <div class="col-lg-8 text-center text-lg-start">
                        <h1 data-aos="fade-right">Bienvenue au cabinet BH Consulting</h1>
                        <p class="mb-5" data-aos="fade-right" data-aos-delay="100">Nous vous offrons les meilleurs
                            services. <br>
                            votre efficacité, notre priorité
                        </p>
                        <p data-aos="fade-right" data-aos-delay="200" data-aos-offset="-500">
                            <a href="{{ route('apropos')}}" class="btn btn-outline-white">A propos de nous</a>
                            <a href="{{ route('contact')}}" class="btn btn-outline-white">Contactez-nous</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hero -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-md-5" data-aos="fade-up">
                <h2 class="section-heading">Nos comptences</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3" data-aos="fade-up" data-aos-delay="">
                <div class="feature-1 text-center">
                    <div class="wrap-icon icon-1">
                        <img src="assets/img/manager.png" alt="Image" class="img-fluid">
                    </div>
                    <h3 class="mb-3">Gestion des entreprises</h3>
                    <p>Nous vous aidons à identifier les actions de nature à conforter la stratégie, à en piloter la
                        mise en œuvre opérationnelle et, enfin, à en contrôler les résultats de votre entreprise</p>
                </div>
            </div>
            <div class="col-md-3" data-aos="fade-up" data-aos-delay="200">
                <div class="feature-1 text-center">
                    <div class="wrap-icon icon-1">
                        <img src="assets/img/mine-icone.png" alt="Image" class="img-fluid">
                    </div>
                    <h3 class="mb-3">Mine et carriere</h3>
                    <p>Nous vous accompagnons dans l'exploitation miniere et egalement vous fournir les agregats
                        necessaires à vos construction.</p>
                </div>
            </div>
            <div class="col-md-3" data-aos="fade-up" data-aos-delay="200">
                <div class="feature-1 text-center">
                    <div class="wrap-icon icon-1">
                        <img src="assets/img/iconeimmo.png" alt="Image" class="img-fluid">
                    </div>
                    <h3 class="mb-3">Immobilier</h3>
                    <p>Nous vous offrons des services de location, de vente de batiment tout genre à conakry et à
                        l'interieur du pays, mais aussi des terrains avec garantie et securité</p>
                </div>
            </div>
            <div class="col-md-3" data-aos="fade-up" data-aos-delay="200">
                <div class="feature-1 text-center">
                    <div class="wrap-icon icon-1">
                        <img src="assets/img/communication.png" alt="Image" class="img-fluid">
                    </div>
                    <h3 class="mb-3">Communication et événementiel</h3>
                    <p>Notre equipe vous accompagne dans l'orgnaisation et la communication sur vos évènenemts, vous
                        accompagen dans le marketing des activités de votre entreprise</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="row justify-content-center text-center mb-5">
        <div class="col-md-5" data-aos="fade-up">
            <h2 class="section-heading">Nos dernieres annonces</h2>
        </div>
    </div>
    <div class="container">
        <div class="row mb-5">
            @foreach ($articles as $article)
            <div class="col-md-4 mt-2">
                <div class="card">
                    <img src="{{asset('/storage/images/articles/'.$article->image)}}" alt=""
                        style="width: 100%; height:200px;">
                    <div class="card-body">
                        <h2>{{$article->title}}</h2>
                        <div class="post-entry">

                            <div class="post-text">
                                <p>{{Str::limit($article->description, 100)}}</p>
                            </div>
                        </div>
                        <p><a href=" {{ route('article', $article->slug) }}" class="btn btn-success text-white"
                                style="margin-left: 1rem; margin-top:0;">Voir plus</a></p>
                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </div>
    <section>
        <div class="d-flex jusitfy-content-center">
            <div class="col-12" data-aos="fade-right" data-aos-delay="100">
                <div>
                    <h3 style="text-align: center;">Avez vous besoins d'autres chauses, <a
                            href=" {{route ('newcontact')}}" class="">Cliquez ici</a></h3>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
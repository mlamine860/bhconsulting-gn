@extends('base')
@section('content')
 <main id="main">

  <!-- ======= Blog Section ======= -->
  <section class="hero-section inner-page">
    <div class="wave">

      <svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
            <path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z" id="Path"></path>
          </g>
        </g>
      </svg>

    </div>

    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="row justify-content-center">
            <div class="col-md-7 text-center hero-text">
              <h1 data-aos="fade-up" data-aos-delay="">Nos services</h1>
              <p class="mb-5" data-aos="fade-up" data-aos-delay="100">Ici vous trouverez l'ensemble des services que nous vous offrons.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <section class="section">
      <div class="container">
        <div class="row mb-5">
          @foreach ($services as $service)
            <div class="col-md-3">
              <div class="post-entry">
                <div class="post-text">
                  <h2>{{$service->nom}}</h2>
                  <p>{{Str::limit($service->description, 100)}}</p>
                  <p><a href="{{ route('service', $service->slug) }}" class="btn btn-success text-white">Voir plus</a></p>
                </div>
              </div>
            </div>
          @endforeach
        </div>
      </div>
  </section>
  <div class="container">
      <div class="col-12 col-md-6 mb-5" data-aos="fade-up">
        <script>
          if (document.getElementById('contact-form').submit()) {
          alert("Message bien envoyé");
          }
        </script>
        <div class="">
          <form action=" {{ route('commentaireStore') }}" method="POST" id="contact-form">
            @csrf
            <select class="form-select form-control my-3" style="width: 10rem;" name="service" aria-label="Default select example">
                <label>Services</label>
                @foreach ($services as $service)
                    <option value="{{$service->id}}">{{$service->nom}}</option>
                @endforeach
            </select> 
            <div class="row">
              <div class="form-group has-error col-md-6">
                  <label for="nom" class="control-label">Nom complet</label>
                  <input type="text" name="nom" class="form-control" required>
                  @error('nom')
                    <span class="invalid-feedback" role="alert">
                        <strong> {{$message}} </strong>
                    </span>
                  @enderror
              </div>
              <div class="form-group has-error col-md-6">
                  <label for="email" class="control-label">E-mail</label>
                  <input type="email" name="email" class="form-control" required>
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong> {{$message}} </strong>
                    </span>
                  @enderror
              </div>
            </div>
            <div class="form-group has-error">
                <label for="telephone" class="control-label">Téléphone</label>
                <input type="text" name="telephone" class="form-control" required>
                @error('telephone')
                  <span class="invalid-feedback" role="alert">
                      <strong> {{$message}} </strong>
                  </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="message" class="control-label">Commentaire</label>
                <textarea name="commentaire" class="form-control" id="" cols="30" rows="10"></textarea>
            </div>
            <div>
                <input type="submit" id="submit" value="Envoyer" class="btn btn-success mt-3">
            </div>
          </form>
        </div>
    </div>
  </div>
  </main><!-- End #main -->
@endsection

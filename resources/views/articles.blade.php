@extends('base')
@section('content')
<main id="main">

  <!-- ======= Blog Section ======= -->
  <section class="hero-section inner-page">
    <div class="wave">

      <svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
            <path
              d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z"
              id="Path"></path>
          </g>
        </g>
      </svg>

    </div>

    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="row justify-content-center">
            <div class="col-md-7 text-center hero-text">
              <h1 data-aos="fade-up" data-aos-delay="">Nos articles</h1>
              <p class="mb-5" data-aos="fade-up" data-aos-delay="100">Parcourez la liste de tous les articles, trouvez
                votre convenance et contactez-nous</p>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>
  <section class="section">
    <div class="container">
      <div class="row mb-5">
        @foreach ($articles as $article)
        <div class="col-md-4 mt-2">
          <div class="card">
            <img src="{{asset('/storage/images/articles/'.$article->image)}}" class="card-img" alt="{{$article->name}}">
            <div class="card-body">
              <h2>{{$article->title}}</h2>
              <div class="">
                <div class="post-entry">

                  <div class="post-text">
                    <p>{{Str::limit($article->description, 100)}}</p>
                  </div>
                </div>
              </div>
              <div class="strong mb-4">{{$article->price}}FGN</div>
              <a href=" {{ route('article', $article->slug) }}" class="btn btn-success text-white">Voir
                plus</a>
            </div>

          </div>
        </div>


        @endforeach
      </div>
      <div class="container">
        <div class="row mt-3">
          <div class="col d-flex justify-content-center">
            {{ $articles -> links('vendor.pagination.custom') }}
          </div>
        </div>
      </div>
    </div>
  </section>
</main><!-- End #main -->
@endsection
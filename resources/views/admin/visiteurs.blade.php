@extends('admin.base')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Liste des visiteurs</h3>                                        
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>TITRE</th>
                            <th>DATE DE CREATION</th>
                            <th class="w-auto">ACTION</th>
                        </tr>
                    </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td><a href="#"></a></td>
                                <td></td>
                                <td class="d-flex float-right">
                                    <a href="" class="btn btn-success">Edit</a>
                                    <form class="ml-2" action="" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection      

@extends('admin.base')

@section('content')
    <!-- ======= Single Blog Section ======= -->
    <section class="site-section mb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8 blog-content">
                    <h2>{{$article->title}}</h2>
                    <div>
                        {{$article->description}}
                    </div> 

                    <div class="pt-5">
                        <div class="d-flex comment-form-wrap pt-5">
                            <form class="mr-2" action="{{ route('articles.delete', $article->id)}}" method="POST">
                                @csrf
                                @method("DELETE")
                                <button type="submit" class="btn btn-danger">Supprimer</button>
                            </form>
                            <a href="{{ route('articles.edit', $article->id)}}" class="btn btn-success">Modifier</a>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 mt-5">
                    <img src="{{asset('/storage/images/articles/'.$article->image)}}" alt="je suis une image" class="w-100 h-100">
                </div>
            </div>
        </div>
    </section>
@endsection
@extends('admin.base')

@section('content')
<div class="">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-success ">
                        <div class="card-header">
                            <h3 class="card-title">Entrer les informations</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('serviceStore')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nom">Titre</label>
                                    <input type="text" name="nom" value="{{old('nom')}}"
                                        class="form-control {{ $errors->has('nom') ? 'is-invalid' : ''}}" id="nom"
                                        placeholder="Entrer votre nom complet">
                                    @if($errors->has('nom'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('nom')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="" cols="30" rows="10"
                                        class="form-control {{ $errors->has('description') ? 'is-invalid' : ''}}">{{old('description')}}</textarea>
                                    @if($errors->has('description'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('description')}}
                                    </div>
                                    @endif
                                </div>
                                <select class="form-select form-control my-3" name="categorie_id"
                                    aria-label="Default select example">
                                    <label>Catégorie</label>
                                    @foreach ($categories as $categorie)
                                    <option value="{{$categorie->id}}">{{$categorie->categories}}</option>
                                    @endforeach
                                </select>
                                <div class="form-group">
                                    <label for="">Image</label>
                                    <input type="file" name="image" class="" placeholder="Ajouter votre image ici">
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Ajouter</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
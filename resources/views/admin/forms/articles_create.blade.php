@extends('admin.base')

@section('content')
<div class="">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-success ">
                        <div class="card-header">
                            <h3 class="card-title">Entrer les informations</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('articleStore')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Titre</label>
                                    <input type="text" name="title" value="{{old('title')}}"
                                        class="form-control {{ $errors->has('title') ? 'is-invalid' : ''}}" id="title"
                                        placeholder="Entrer le titre de l'article">
                                    @if($errors->has('title'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('title')}}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="price">Prix</label>
                                    <input type="text" name="price" value="{{old('price')}}" class="form-control {{ $errors->has('price') ?
                                    'is-invalid' : ''}}" id="price" placeholder="Entrer le prix de l'article">
                                    @if($errors->has('price'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('price')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea id="description" name="description" class="form-control w-100 {{$errors->has('description') ?
                                    'is-invalid' : ''}}">{{old('description')}}</textarea>
                                    @if($errors->has('price'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('description')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <select class="form-select form-control my-3 {{$errors->has('categorie') ?
                                                                    'is-invalid' : ''}} " style="width: 10rem;"
                                        name="categorie" aria-label="Default select example">
                                        @foreach ($categories as $categorie)
                                        <option value="{{$categorie->id}}">{{$categorie->categories}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('price'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('categorie')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Choisir une image</label>
                                    <input type="file" name="image" class="" placeholder="Ajouter votre image ici">
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Ajouter</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
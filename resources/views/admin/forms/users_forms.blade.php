@extends('admin.base')

@section('content')
    <div class="">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-12">
                        <!-- general form elements -->
                        <div class="card card-success ">
                            <div class="card-header">
                                <h3 class="card-title">Entrer les informations</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action=" {{ route('userStore')}}" method="POST">                              
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="nom">Nom complet</label>
                                        <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                                            placeholder="Entrer le nom complet de l'utlisateur">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Mail</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Entrer votre mail">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Mot de passe</label>
                                        <input type="password" name="password" class="form-control" id="exampleInputPassword1"
                                            placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="confirme">Confirme</label>
                                        <input type="password" name="password_confirm" class="form-control" id="exampleInputPassword1"
                                            placeholder="Confirme">
                                    </div>                                    
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-success">Ajouter</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@extends('admin.base')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Liste des articles</h3>
                        <div class="clearfix">
                            <a href="{{route('articleCreate')}}" class="btn btn-success btn-md ml-3 float-right">
                                Nouveau
                                <i class="fas fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>TITRE</th>
                                    <th>PRIX</th>
                                    <th>DATE DE CREATION</th>
                                    <th>IMAGE</th>
                                    <th class="w-auto">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($articles as $article)
                                <tr>
                                    <td>{{ $article->id }}</td>
                                    <td><a href=" {{ route('articleDetail', $article->slug) }}">{{ $article->title
                                            }}</a></td>
                                    <td>{{$article->price}}</td>
                                    <td>{{ $article->created_at }}</td>
                                    <td><img src="{{asset('/storage/images/articles/'.$article->image)}}" alt=""
                                            style="height: 2rem; width:2 rem;"></td>
                                    <td class="d-flex float-right">
                                        <a href="{{ route('articles.edit', $article->id)}}"
                                            class="btn btn-success">Edit</a>
                                        <form class="ml-2" action="{{ route('articles.delete', $article->id)}}"
                                            method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" class="btn btn-danger">Supprimer</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="container">
                            <div class="row mt-3">
                                <div class="col d-flex justify-content-center">
                                    {{ $articles -> links('vendor.pagination.custom') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</section>
@endsection
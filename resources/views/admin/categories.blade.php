@extends('admin.base')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Liste des categories</h3>
                    <div class="clearfix">
                       <form method="POST" action="{{ route('categorieStore')}}" enctype="multipart/form-data"  class="row float-right">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="categorie" class="form-control" id="exampleInputEmail1"
                                    placeholder="Entrer la categorie">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Ajouter</button>
                            </div>
                        </form>
                    </div>                                        
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>TITRE</th>
                            <th class="w-auto">ACTION</th>
                        </tr>
                    </thead>
                        <tbody>
                        @foreach ($categories as $categorie)
                            <tr>
                                <td>{{ $categorie->id }}</td>
                                <td>{{ $categorie->categories}}</td>
                                <td class="d-flex float-right">
                                    <a href="{{ route('categories.edit', $categorie->id)}}" class="btn btn-success">Edit</a>
                                    <form class="ml-2" action="{{ route('categories.delete', $categorie->id)}}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit" class="btn btn-danger">Supprimer</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection      

@extends('admin.base')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Liste des articles</h3>                                       
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>NOM</th>
                            <th>TELEPHONE</th>
                            <th>EMAIL</th>
                            <th>COMMENTAIRE</th>
                            <th>SERVICES</th>
                        </tr>
                    </thead>
                        <tbody>
                        @foreach ($commentaires as $commentaire)
                            <tr>
                                <td>{{$commentaire->id }}</td>
                                <td><a href="">{{ $commentaire->nom }}</a></td>
                                <td>{{ $commentaire->telephone }}</td>
                                <td>{{ $commentaire->email }}</td>
                                <td>{{ Str::limit($commentaire->commentaire, 40) }}</td>
                                <td>{{ $commentaire->services_id }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                </table>
                <div class="container">
                    <div class="row mt-3">
                    <div class="col d-flex justify-content-center">
                        {{ $commentaires -> links('vendor.pagination.custom') }}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection      

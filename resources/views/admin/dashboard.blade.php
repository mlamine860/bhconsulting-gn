@extends('admin.base')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
           <div class="card">
                <div class="card-header bg-success">
                   <h1>Statistiques</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box">
                            <div class="inner">
                                <h3>{{$articlesCount}}</h3>
                                <p>Articles</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{ route('adminArticle') }}" class="small-box-footer bg-success">Voir plus<i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box">
                            <div class="inner">
                                <h3>{{$servicesCount}}</h3>
                                <p>Services</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{ route('adminService')}}" class="small-box-footer bg-success">Voir plus <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box">
                            <div class="inner">
                                <h3>{{$userCount}}</h3>
                                <p>Utilisateurs</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{route('users')}}" class="small-box-footer bg-success">Voir plus<i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-3 col-6">
                            <!-- small box -->
                            <div class="small-box">
                            <div class="inner">
                                <h3>{{$contactCount}}</h3>
                                <p>Visiteurs</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href=" {{route('visiteurs')}}" class="small-box-footer bg-success">Voir plus <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            <!-- ./col -->
            </div>
            <!-- /.row -->
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->
        <div class="col-12">
            <div class="">
                <div class="">
                    <div class="card card-row card-success">
                        <div class="card-header">
                            <div class="card-title">
                                <h2>Services</h2>
                            </div>
                        </div>
                        <div class="vertical-scroll">
                            <div class="card-body">
                                <table  class="col-12 table table-striped table-wrapper">
                                    <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Titre</th>
                                                <th>Date de création</th>
                                                <th>Action</th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($services as $service)
                                            <tr>
                                                <td>{{$service->id}}</td>
                                                <td>
                                                    {{$service->nom}}
                                                </td> 
                                                <td>{{$service->created_at}}</td>
                                                <td><a href="{{ route('serviceDetail', $service->slug) }}" class="btn btn-outline-success"> Detail</a></td>                                      
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="container">
                                    <div class="row mt-3">
                                        <div class="col d-flex justify-content-center">
                                            {{ $services -> links('vendor.pagination.custom') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@extends('admin.base')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Les Services</h3>
                    <div class="clearfix">
                        <a href=" {{route('serviceCreate')}}" class="btn btn-success btn-md ml-3 float-right">
                            Nouveau
                            <i class="fas fa-plus"></i>
                        </a>                                           
                    </div>                  
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Titre</th>
                            <th>Image</th>
                            <th>Date de création</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach ($services as $service)
                                <tr>
                                    <td>{{ $service->id }}</td>
                                    <td><a href=" {{ route('serviceDetail', $service->slug) }}" class="text-success">{{ $service->nom }}</a></td>
                                    <td><img src="{{asset('/storage/images/services/'.$service->image)}}" alt="" style="height: 2rem; width:2 rem;"></td>
                                    <td>{{ $service->created_at }}</td>
                                    <td class="d-flex float-right ">
                                        <a href=" {{route('services.edit', $service->id)}}" class="btn btn-success">Edit</a>
                                        <form class="ml-2" action="{{ route('services.delete', $service->id)}}" method="POST">
                                            @csrf
                                            @method("DELETE")
                                            <button type="submit" class="btn btn-danger">Supprimer</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection      

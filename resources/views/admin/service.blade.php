@extends('admin.base')

@section('content')
    
    <!-- ======= Single Blog Section ======= -->

    <section class="site-section mb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8 blog-content">

                    <h2>{{$service->nom}}</h2>

                    <div>
                        {{$service->description}}
                    </div>

                    <div class="pt-5">
                        <div class="comment-form-wrap pt-5">
                            <a href="" class="btn btn-danger">Supprimer</a>
                            <a href="" class="btn btn-success">Modifier</a>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4">
                    <img src="" alt="je suis une image">
                </div>
            </div>
        </div>
    </section>
@endsection


@extends('admin.base')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">Liste des utilisateurs</h1>
                        <div class="clearfix">
                            <a href="{{route('userCreate')}}" class="btn btn-success btn-md ml-3 float-right">
                                Nouveau
                                <i class="fas fa-plus"></i>
                            </a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>NOM</th>
                                    <th>EMAIL</th>
                                    <th>ADMIN</th>
                                    <th>SUPPRIMEZ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if(auth()->user()->id != $user->id)
                                        @if($user->admin)
                                        <a href="{{route('user.toggleAdmin', $user)}}" class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                        @else
                                        <a href="{{route('user.toggleAdmin', $user)}}" class="btn btn-warning btn-sm"><i
                                                class="fas fa-check"></i></a>
                                        @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if(auth()->user()->id != $user->id)
                                        <a href="{{route('user.destroy', $user)}}" class="btn btn-danger btn-sm">
                                            Supprimez cet utilisateur
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
</section>
@endsection
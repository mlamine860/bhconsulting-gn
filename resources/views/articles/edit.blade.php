@extends('admin.base')

@section('content')
<div class="">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-12">
                    <!-- general form elements -->
                    <div class="card card-success ">
                        <div class="card-header">
                            <h3 class="card-title">Modifer les informations</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('articles.update', $articles->id)}}"
                            enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            {{-- <div class="card-body">
                                <div class="form-group">
                                    <label for="">Titre</label>
                                    <input type="text" value="{{$articles->title}}" name="title" class="form-control"
                                        id="exampleInputEmail1" placeholder="Entrer votre nom complet">
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="" cols="30" rows="10"
                                        class="form-control">{{$articles->description}}</textarea>
                                </div>
                                <select class="form-select form-control my-3" style="width: 10rem;" name="categorie"
                                    aria-label="Default select example">
                                    <option selected>Catégorie</option>
                                    @foreach ($categories as $categorie)
                                    <option value="{{$categorie->id}}" {{$categorie->id === $articles->categorie_id ?
                                        'selected' : ''}}>{{$categorie->categories}}</option>
                                    @endforeach
                                </select>
                                <div class="form-group">
                                    <label for="">Image</label>
                                    <input type="file" name="image" class="" placeholder="Ajouter votre image ici">
                                </div>
                            </div> --}}
                            <!-- /.card-body -->
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Titre</label>
                                    <input type="text" name="title" value="{{old('title', $articles->title)}}"
                                        class="form-control {{ $errors->has('title') ? 'is-invalid' : ''}}" id="title"
                                        placeholder="Entrer le titre de l'article">
                                    @if($errors->has('title'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('title')}}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="price">Prix</label>
                                    <input type="text" name="price" value="{{old('price', $articles->price)}}" class="form-control {{ $errors->has('price') ?
                                                                'is-invalid' : ''}}" id="price"
                                        placeholder="Entrer le prix de l'article">
                                    @if($errors->has('price'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('price')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea id="description" name="description"
                                        class="form-control w-100 {{$errors->has('description') ?
                                                                'is-invalid' : ''}}">{{old('description', $articles->description)}}</textarea>
                                    @if($errors->has('price'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('description')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <select class="form-select form-control my-3 {{$errors->has('categorie') ?
                                                                                                'is-invalid' : ''}} "
                                        style="width: 10rem;" name="categorie" aria-label="Default select example">
                                        @foreach ($categories as $categorie)
                                        <option value="{{$categorie->id}}" {{$articles->categorie->id == $categorie->id
                                            ? 'selected' : ''}}>{{$categorie->categories}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('price'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('categorie')}}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="">Choisir une image</label>
                                    <input type="file" name="image" class="" placeholder="Ajouter votre image ici">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Modifer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
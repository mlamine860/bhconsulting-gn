@extends('base')
@section('content')
   <!-- ======= Features Section ======= -->
    <section class="hero-section inner-page">
      <div class="wave">
        <svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
              <path d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z" id="Path"></path>
            </g>
          </g>
        </svg>

      </div>

      <div class="container">
        <div class="row align-items-center">
          <div class="col-12">
            <div class="row justify-content-center">
              <div class="col-md-7 text-center hero-text">
                <h1 data-aos="fade-up" data-aos-delay="">A propos de nous</h1>
                <p class="mb-5" data-aos="fade-up" data-aos-delay="100">Decouvrez qui nous sommes et contactez-nous pour tout vos besoin</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>
    <section class="section pb-0">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-4 me-auto">
            <h2 class="mb-4">BH Consulting</h2>
            <p class="mb-4">
              <strong>BH CONSULTING</strong> est un cabinet d'experets en Economie et gestion d'entreprise en Republique de Guinée a 
              été crée en 2021, et devenu opérationnel en 2022. Propose des solutions adaptées aux problèmes spécifiques de chaque 
              entreprise.
              En effet, le cabinet est avant tout une equipe de consultants pluridisciplinaires qui peuvent travailler
              dans plusieurs domaines comme: <a href="#">la creation et gestion d'entreprise</a>, <a href="">l'informatique</a>,
              <a href="">la maintenance reseau</a>, <a href="">la programmation</a>, <a href="">la logistique</a>, <a href="">l'immobilier</a>,
              <a href="">les ressources humaines</a>, <a href="">le management</a>, <a href="">la formation et l'enquête</a>,
              <a href="">le consulting...</a>
            </p>
            <p><a class="btn btn-success" href="{{route('contact')}}">prenez un rendez-vous</a></p>
          </div>
          <div class="col-md-6" data-aos="fade-left">
            <img src="assets/img/logo.jpg" alt="Image" class="img-fluid">
          </div>
        </div>
      </div>
    </section>

    {{-- <section class="section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-4 ms-auto order-2">
            <h2 class="mb-4">Nos partenaires</h2>
            <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. 
              Omnis a, dicta recusandae minus reprehenderit dolorem iste asperiores aperiam facere voluptas cum dignissimos optio harum consectetur rerum accusantium itaque cumque ullam.</p>
              <ul>
                <li><a href=""><h3>Lorem ipsum dolor sit amet.</h3></a></li>
                <li><a href=""><h3>Lorem ipsum dolor sit amet.</h3></a></li>
                <li><a href=""><h3>Lorem ipsum dolor sit amet.</h3></a></li>
                <li><a href=""><h3>Lorem ipsum dolor sit amet.</h3></a></li>
                <li><a href=""><h3>Lorem ipsum dolor sit amet.</h3></a></li>
              </ul>
          </div>
          <div class="col-md-6" data-aos="fade-right">
            <img src="assets/img/undraw_svg_3.svg" alt="Image" class="img-fluid">
          </div>
        </div>
      </div>
    </section> --}}
    <!-- ======= CTA Section ======= -->
@endsection

<!-- ======= Header ======= -->

</body>

</html>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>BH-Consulting</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ url('assets/img/favicon.png')}}" rel="icon">
  <link href="{{ url('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link
    href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{ url('assets/vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{ url('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{ url('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{ url('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{ url('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="{{ url('assets/css/style.css')}}">
  <!-- =======================================================
  * Template Name: SoftLand - v4.7.0
  * Template URL: https://bootstrapmade.com/softland-bootstrap-app-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">
    <div class=" navbar bg-dark container d-flex justify-content-between align-items-center px-4"
      style="border-radius: 2rem;">
      <div class="logo">
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="{{route('index')}}"><img src="{{url('assets/img/logo.png')}}" alt="" class="img-fluid"></a>
      </div>
      <nav id="navbar" class="navbar">
        <ul style="margin-right: 3rem;">
          <li><a class="active " href="{{route ('index')}}">Acceuil</a></li>
          <li><a href="{{ route ('articles')}}">Offres</a></li>
          <li class="dropdown"><a href=" {{route ('services')}}"><span>Services</span> <i
                class="bi bi-chevron-down"></i></a>
            <ul class="dropdown-menu">
              @foreach ($services as $service)
              <li class="dropdown-item"><a href="{{ route('service', $service->slug) }}">{{$service->nom}}</a></li>
              @endforeach
            </ul>
          </li>
          <li class="dropdown"><a href="{{route ('apropos')}}"> <span>Informations</span><i
                class="bi bi-chevron-down"></i></i></a>
            <ul class="dropdown-menu">
              <li class="dropdown-item"><a href="{{ route('apropos')}}"><span>A propos</span></a></li>
              <li class="dropdown-item"><a href="{{ route('contact')}}"><span>Contactez-nous</span></a></li>
              <li class="dropdown-item"><a href="{{ route('contact')}}"><span>Team</span></a></li>
            </ul>
          </li>
          @if (Auth::user())
          @if(Auth::user()->admin)
          <a href=" {{route('dashboard')}}">Tableau de bord</a>
          @endif
          <a class="text-white" href="{{ route('logout') }} " onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
            Deconnexion
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
          @else
          <a class="text-white" href="{{ route('login') }}"> Connexion </a>
          @endif
        </ul>
        <i class="bi bi-list mobile-nav-toggle" id="navbar"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->
  <main id=main>

    @yield('content')
    <!--Content block-->
    <section class="section cta-section bg-success">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6 me-auto text-center text-md-start mb-5 mb-md-0">
            <h2>Suivez nous aussi sur</h2>
          </div>
          <div class="col-md-5 text-center text-md-end">
            <p><a href="#" class="btn d-inline-flex align-items-center"><i
                  class="bx bxl-facebook"></i><span>facebook</span></a> <a href="#"
                class="btn d-inline-flex align-items-center"><i class="bx bxl-instagram"></i><span>Instagram</span></a>
            </p>
          </div>
        </div>
      </div>
    </section><!-- End CTA Section -->

  </main>
  <!--Footer-->
  <footer class="footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="col-md-4 mb-4 mb-md-0">
          <h3>Apropos de nous</h3>
          <p>
            BH CONSULTING</strong> est un cabinet d'experets en Economie et gestion d'entreprise en Republique de Guinée
            a
            été crée en 2021, et devenu opérationnel en 2022. Propose des solutions adaptées aux problèmes spécifiques
            de chaque
            entreprise.
          </p>
          <p class="social">
            <a href="#"><span class="bi bi-twitter"></span></a>
            <a href="#"><span class="bi bi-facebook"></span></a>
            <a href="#"><span class="bi bi-instagram"></span></a>
            <a href="#"><span class="bi bi-linkedin"></span></a>
          </p>
        </div>
        <div class="col-md-7 ms-auto">
          <div class="row site-section pt-0">
            <div class="col-md-6 mb-4 mb-md-0">
              <h3>Navigation</h3>
              <ul class="list-unstyled">
                <li><a href="{{ route ('index')}}">Acceuil</a></li>
                <li><a href="{{ route ('articles')}}">Offres</a></li>
                {{-- <li><a href="{{ route ('services')}}">Services</a></li> --}}
                <li><a href="{{ route ('contact')}}">Contact</a></li>
                <li><a href=" {{ route ('apropos')}}">A propos</a></li>
              </ul>
            </div>
            <div class="col-md-6 mb-4 mb-md-0">
              <h3>Services</h3>
              <ul class="list-unstyled">
                @foreach ($categories as $categorie)
                <li><a href="#">{{$categorie->categories}}</a></li>
                @endforeach

              </ul>
            </div>
          </div>
        </div>

        <div class="row justify-content-center text-center bg-light">
          <div class="col-md-7">
            <p class="copyright">&copy; BH consultin</p>
            <div class="credits">
              Cabinet d'etude, de conseil et d'expertise-site web developpé par <strong
                class="text-warning">HATIC</strong>
            </div>
          </div>
        </div>

      </div>
  </footer>

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
      class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ url('assets/vendor/aos/aos.js')}}"></script>
  <script src="{{ url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ url('assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{ url('assets/vendor/php-email-form/validate.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ url('assets/js/main.js')}}"></script>
  <!--end footer-->
</body>


</html>
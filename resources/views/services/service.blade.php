@extends('base')
@section('content')
<main id="main">

  <!-- ======= Single Blog Section ======= -->
  <section class="hero-section inner-page">
    <div class="wave">
      <svg width="1920px" height="265px" viewBox="0 0 1920 265" version="1.1" xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
            <path
              d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,667 L1017.15166,667 L0,667 L0,439.134243 Z"
              id="Path"></path>
          </g>
        </g>
      </svg>
    </div>

    </div>

    <div class="container">
      <div class="row align-items-center">
        <div class="col-12">
          <div class="row justify-content-center">
            <div class="col-md-10 text-center hero-text">
              <h1 data-aos="fade-up" data-aos-delay="">{{$service->nom}}</h1>
              <a href="{{route ('services')}}" class="pt-3 btn btn-light text-dark"> <i
                  class="fas fa-arrow">retour</i></a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>

  <section class="site-section mb-4">
    <div class="container">
      <div class="row">
        <div class="col-md-8 blog-content">

          <h2>{{$service->nom}}</h2>

          <blockquote>
            {{$service->description}}
          </blockquote>

          <div class="pt-5">
            <div class="comment-form-wrap pt-5">
              <a href="{{route('contact')}}" class="btn btn-success">Contactez-nous pour ce service</a>
            </div>
          </div>
        </div>
        <div class="col col-md-4">
          <img src="{{asset('/storage/images/services/'.$service->image)}}" alt="{{$service->nom}}" class="w-100">
        </div>
      </div>
    </div>
  </section>

</main><!-- End #main -->
@endsection
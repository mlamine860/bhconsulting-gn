<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin Dashbord',
            'email' => 'admin.dashboard@bhconsulting-gn.com',
            'password' => Hash::make('SW(GU+pIpG(c^43z'),
            'admin' => 1

        ]);
    }
}

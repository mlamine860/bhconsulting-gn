<?php

namespace Database\Seeders;

use App\Models\Categorie;
use Illuminate\Database\Seeder;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Immobilier', 'Mine', 'Gestion entreprise'];
        foreach ($categories as $categorie) {
            Categorie::create([
                'categories' => $categorie
            ]);
        }
    }
}

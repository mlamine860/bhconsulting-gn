<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Services;
use App\Models\Commentaire;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = Articles::all();
        $commentaires = Commentaire::all();
        $services = Services::all();
        return view('home',
            compact('services', 'commentaires', 'articles')
        );
    }
    public function comment(){
        $commentaires = Commentaire::all();
        return view('index', compact('commentaires'));
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Articles;
use App\Models\Categorie;
use App\Models\Commentaire;

class ArticleController extends Controller
{
    use StoreImageTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commentairesCount = Commentaire::count('id');
        $articles = Articles::paginate(30);
        return view('admin.articles', compact('articles', 'commentairesCount'));
    }

    public function voirplus($slug)
    {
        $commentairesCount = Commentaire::count('id');
        $article = Articles::where('slug', $slug)->firstOrFail();
        return view('admin.article', compact('article', 'commentairesCount'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $commentairesCount = Commentaire::count('id');
        $categories = Categorie::all();
        return view('admin.forms.articles_create', compact('categories', 'commentairesCount'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $articles =  new Articles();
        if ($request->hasFile('image')) {
            $this->uploadImage($articles, $request->file('image'), 'articles');
        }
        $articles->title = $request->title;
        $articles->price = $request->price;
        $articles->description = $request->description;
        $articles->categorie_id = $request->categorie;
        $articles->save();

        return redirect()->route('adminArticle')->with('success', "L'article a bien été ajouté !");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Articles $articles)
    {
        $categories = Categorie::all();
        $commentairesCount = Categorie::count('id');
        return view('articles.edit', compact('articles', 'commentairesCount', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Articles $articles)
    {
        $articles->title = $request->title;
        $articles->description = $request->description;
        $articles->price = $request->price;
        $articles->categorie_id = $request->categorie;
        if ($request->hasFile('image')) {
            $this->uploadImage($articles, $request->file('image'), 'articles');
        }
        $articles->save();

        return redirect()->route('adminArticle')->with('success', "L'article a bien été modifié !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Articles $articles)
    {
        $articles->delete();
        return redirect()->route('adminArticle')->with('success', "L'article a bien été supprimé !");
    }
}

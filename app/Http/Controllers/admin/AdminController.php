<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use App\Models\Contact;
use App\Models\Articles;
use App\Models\Services;
use App\Models\Visiteurs;
use App\Models\Commentaire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        // $articlesCount = Articles::count('id');
        // $servicesCount = Services::count('id');
        // $userCount = User::count('id');
        // $services = Services::paginate(2);
        // $commentairesCount = Commentaire::count('id');
        // $contactCount = Contact::count('id');
        // $data = compact('articlesCount', 'servicesCount', 'contactCount', 'userCount', 'services', 'commentairesCount');
        return view('admin.dashboard');
    }
}

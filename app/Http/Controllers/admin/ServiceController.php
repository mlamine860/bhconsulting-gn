<?php

namespace App\Http\Controllers\admin;

use App\Models\Services;
use App\Models\Commentaire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequest;
use App\Models\Categorie;

class ServiceController extends Controller
{
    use StoreImageTrait;


    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commentairesCount = Commentaire::count('id');
        $services = Services::all();
        return view('admin.services', compact('services', 'commentairesCount'));
    }
    public function voirplus($slug)
    {
        $commentairesCount = Commentaire::count('id');
        $service = Services::where('slug', $slug)->firstOrFail();
        return view('admin.service', compact('service', 'commentairesCount'));
    }

    public function service()
    {
        $commentairesCount = Commentaire::count('id');
        $categories = Categorie::all();
        return view('admin.forms.service', compact('commentairesCount', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $service = new Services();
        if ($request->hasFile('image')) {
            $this->uploadImage($service, $request->file('image'), 'services');
        }
        $service->nom = $request->nom;
        $service->description = $request->description;
        $service->categorie_id = $request->categorie_id;

        $service->save();

        return redirect()->route('adminService')->with('success', "L'article a bien été enregistré");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $services)
    {
        $categories = Categorie::all();
        $commentairesCount = Categorie::count();
        return view('services.edit', compact('services', 'categories', 'commentairesCount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, Services $services)
    {
        $services->nom = $request->nom;
        $services->categorie_id = $request->categorie_id;
        $services->description = $request->description;
        if ($request->hasFile('image')) {
            $this->uploadImage($services, $request->file('image'), 'services');
        }
        $services->save();

        return redirect()->route('adminService')->with('success', "L'article a bien été modifié !");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $services)
    {
        $services->delete();
        return redirect()->route('adminService')->with('warning', "L'article a bien été supprimé !");
    }
}

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

trait StoreImageTrait
{
    private function uploadImage($model, $uploadedFile, $dir)
    {
        $destination_path = 'public/images/' . $dir;
        $file = $destination_path . '/' . $model->image;
        if (Storage::exists($file)) {
            Storage::delete($file);
        }
        $image_name = md5($uploadedFile->getClientOriginalName()) .
            '.' . $uploadedFile->getClientOriginalExtension();
        $uploadedFile->storeAs($destination_path, $image_name);
        $model->image = $image_name;
    }
}

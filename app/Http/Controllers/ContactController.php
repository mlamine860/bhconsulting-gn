<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Contact;
use App\Models\Services;
use App\Models\Visiteurs;
use App\Models\Commentaire;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contact()
    {
        $services = Services::all();
        $categories = Categorie::all();
        return view('contact', compact('services', 'categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nom_complet' => 'required|min:3',
            'email' => 'required',
            'telephone' => 'required',
            'objet' => 'required',
            'message' => 'required'
        ]);

        $contact = new Contact();

        $contact->nom_complet = $request->nom_complet;
        $contact->email = $request->email;
        $contact->telephone = $request->telephone;
        $contact->objet = $request->objet;
        $contact->message = $request->message;

        $contact->save();

        return redirect()->back()->with('success', 'Votre message a bien été envoié! Nous vous contacterons bientôt.');
    }

    public function newcontact()
    {
        $services = Services::all();
        $categories = Categorie::all();
        return view('newcontact', compact('services', 'categories'));
    }
}

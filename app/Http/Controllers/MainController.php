<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Categorie;
use App\Models\Commentaire;
use App\Models\Services;
use App\Models\Visiteurs;
use Illuminate\Http\Request;

class MainController extends Controller
{

    //
    public function index()
    {
        $services = Services::all();
        $articles = Articles::limit(3)->get();
        $commentaires = Commentaire::latest('id')->first();
        $categories = Categorie::all();
        return view('index', compact('services', 'commentaires', 'articles', 'categories'));
    }
    //-------Articles function-------
    public function article()
    {
        $services = Services::all();
        $articles = Articles::paginate(12);
        $categories = Categorie::all();
        return view('articles', compact('articles', 'services', 'categories'));
    }

    public function voirplus($slug)
    {
        $services = Services::all();
        $article = Articles::where('slug', $slug)->firstOrFail();
        $categories = Categorie::all();
        return view('articles.detailArticle', compact('article', 'services', 'categories'));
    }
    //-----------EndArticles function----------
    //-------Service function-----------

    public function services()
    {
        $services = Services::all();
        $categories = Categorie::all();
        return view('services', [
            'services' => $services,
            'categories' => $categories
        ]);
    }

    public function service($slug)
    {
        $services = Services::all();
        $service = Services::where('slug', $slug)->firstOrFail();
        $categories = Categorie::all();
        return view('services.service', compact('services', 'service', 'categories'));
    }

    //--------EndService function-------------
    public function apropos()
    {
        $services = Services::all();
        $categories = Categorie::all();
        return view("about", compact('services', 'categories'));
    }
    //---------Contact fonction
    public function contact()
    {
        $services = Services::all();
        $categories = Categorie::all();
        dd($categories);
        return view('contact', compact('services', 'categories'));
    }
}

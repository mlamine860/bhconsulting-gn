<?php

namespace App\Models;

use App\Models\Services;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Commentaire extends Model
{
    use HasFactory;
    protected $fillable = ['nom', 'email', 'telephone', 'commentaire', 'service_id'];
    
    public function services()
    {
        return $this->hasMany('App\Models\Services', 'services_id', 'id');
    }
}

<?php

namespace App\Models;

use App\Models\Commentaire;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Services extends Model
{
    use HasFactory;

    protected $fillable = ['nom', 'description', 'image', 'categorie_id'];

    public function commentaires()
    {
        return $this->hasMany('App\Models\Commentaire', 'services_id', 'id');
    }

    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }
}

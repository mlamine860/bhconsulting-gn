<?php

namespace App\Models;

use App\Models\Articles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Categorie extends Model
{
    use HasFactory;
    protected $fillable = [
        'categories'
    ];

    public function articles()
    {
        return $this->hasMany(Articles::class);
    }
    public function services()
    {
        return $this->hasMany(Services::class);
    }
}

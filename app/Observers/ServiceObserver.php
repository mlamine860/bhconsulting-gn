<?php

namespace App\Observers;

use App\Models\Services;
use Cocur\Slugify\Slugify;

class ServiceObserver
{
    /**
     * Handle the Services "created" event.
     *
     * @param  \App\Models\Services  $services
     * @return void
     */
    public function created(Services $services)
    {
        $instance = new Slugify();
        $services->slug=$instance->slugify($services->nom);
        $services->save();
    }

    /**
     * Handle the Services "updated" event.
     *
     * @param  \App\Models\Services  $services
     * @return void
     */
    public function updated(Services $services)
    {
        //
    }

    /**
     * Handle the Services "deleted" event.
     *
     * @param  \App\Models\Services  $services
     * @return void
     */
    public function deleted(Services $services)
    {
        //
    }

    /**
     * Handle the Services "restored" event.
     *
     * @param  \App\Models\Services  $services
     * @return void
     */
    public function restored(Services $services)
    {
        //
    }

    /**
     * Handle the Services "force deleted" event.
     *
     * @param  \App\Models\Services  $services
     * @return void
     */
    public function forceDeleted(Services $services)
    {
        //
    }
}

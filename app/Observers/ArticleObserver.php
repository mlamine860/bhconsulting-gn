<?php

namespace App\Observers;

use App\Models\Articles;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Str;

class ArticleObserver
{
    /**
     * Handle the Articles "created" event.
     *
     * @param  \App\Models\Articles  $articles
     * @return void
     */
    public function created(Articles $articles)
    {
        $articles->slug = Str::slug($articles->title, '-');
        $articles->save();
    }

    /**
     * Handle the Articles "updated" event.
     *
     * @param  \App\Models\Articles  $articles
     * @return void
     */
    public function updated(Articles $articles)
    {
        $articles->slug = Str::slug($articles->title, '-');
        $articles->saveQuietly();
    }

    /**
     * Handle the Articles "deleted" event.
     *
     * @param  \App\Models\Articles  $articles
     * @return void
     */
    public function deleted(Articles $articles)
    {
        //
    }

    /**
     * Handle the Articles "restored" event.
     *
     * @param  \App\Models\Articles  $articles
     * @return void
     */
    public function restored(Articles $articles)
    {
        //
    }

    /**
     * Handle the Articles "force deleted" event.
     *
     * @param  \App\Models\Articles  $articles
     * @return void
     */
    public function forceDeleted(Articles $articles)
    {
        //
    }
}

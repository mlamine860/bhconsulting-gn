<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Contact;
use App\Models\Articles;
use App\Models\Services;
use App\Models\Commentaire;
use Illuminate\Support\Str;
use App\Observers\ArticleObserver;
use App\Observers\ServiceObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Articles::observe(ArticleObserver::class);
        Services::observe(ServiceObserver::class);

        // Share vie

        if (Str::contains(request()->path(), 'admin')) {
            $articlesCount = Articles::count('id');
            $servicesCount = Services::count('id');
            $userCount = User::count('id');
            $services = Services::paginate(2);
            $commentairesCount = Commentaire::count('id');
            $contactCount = Contact::count('id');
            $data = compact('articlesCount', 'servicesCount', 'contactCount', 'userCount', 'services', 'commentairesCount');
            view()->share($data);
        }
    }
}
